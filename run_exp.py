import os
def run_exp(dataset='cifar100', phase=5, gpu=0):

    if dataset=='cifar100':
        if phase==5:
            the_options = 'options/config_cifar100_5phase.yaml'
        elif phase==10:
            the_options = 'options/config_cifar100_10phase.yaml'
        elif phase==25:
            the_options = 'options/config_cifar100_25phase.yaml' 
        else:
            raise ValueError('Please set correct number of phases.')       
    elif dataset=='imagenet_sub':
        if phase==5:
            the_options = 'options/config_imagenet_subset_5phase.yaml'
        elif phase==10:
            the_options = 'options/config_imagenet_subset_10phase.yaml'
        elif phase==25:
            the_options = 'options/config_imagenet_subset_25phase.yaml' 
        else:
            raise ValueError('Please set correct number of phases.')    
    else:
        raise ValueError('Please set correct dataset.')

    the_command = 'python3 -minclearn'
    the_command += ' --options ' + the_options
    the_command += ' --fixed-memory'
    the_command += ' --device ' + str(gpu)
    the_command += ' --save_model'

    the_command += ' 2>&1 | tee ' + 'log_rmm_' + dataset + '_' + str(phase) + 'phase'
    os.system(the_command)

run_exp(dataset='cifar100', phase=5, gpu=0)
run_exp(dataset='cifar100', phase=10, gpu=0)
run_exp(dataset='cifar100', phase=25, gpu=0)
run_exp(dataset='imagenet_sub', phase=5, gpu=0)
run_exp(dataset='imagenet_sub', phase=10, gpu=0)
run_exp(dataset='imagenet_sub', phase=25, gpu=0)
